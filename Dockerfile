FROM python

COPY . /app

RUN pip3 install --trusted-host pypi.python.org -r /app/src/requirements.txt

WORKDIR /app/src


ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:8080", "main:app"]