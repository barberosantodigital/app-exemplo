from flask import Flask, Response, request
import requests
import json
import mimetypes
import flask_cors


app = Flask(__name__)
flask_cors.CORS(app,resources={r"*": {"origins": "*"}})

@app.route("/hello", methods=['GET'])
def hello():
    return Response("Hello Knative v2", mimetype='application/json')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
